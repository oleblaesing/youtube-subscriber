function isOnWatchSite() {
  return /https:\/\/www.youtube.com\/watch/.test(location.href)
}

function isOnChannelSite() {
  return /https:\/\/www.youtube.com\/channel\//.test(location.href)
    || /https:\/\/www.youtube.com\/user\//.test(location.href)
    || /https:\/\/www.youtube.com\/u\//.test(location.href)
}

function getChannel() {
  if (isOnWatchSite()) {
    return document.querySelector('ytd-channel-name.ytd-video-owner-renderer > div:nth-child(1) > div:nth-child(1) > yt-formatted-string:nth-child(1) > a:nth-child(1)')
  }

  if (isOnChannelSite()) {
    return document.querySelector('link[rel="canonical"]')
  }
}

function getChannelId() {
  if (getChannel()) {
    return getChannel().href.slice('https://www.youtube.com/channel/'.length)
  }
}

function getChannelName() {
  if (isOnWatchSite() && getChannel()) {
    return getChannel().innerText
  }

  if (isOnChannelSite() && document.querySelector('ytd-channel-name.ytd-c4-tabbed-header-renderer > div:nth-child(1) > div:nth-child(1) > yt-formatted-string:nth-child(1)')) {
    return document
      .querySelector('ytd-channel-name.ytd-c4-tabbed-header-renderer > div:nth-child(1) > div:nth-child(1) > yt-formatted-string:nth-child(1)')
      .innerText
  }
}

function getSubscribeButton() {
  if (isOnWatchSite()) {
    return document.querySelector('paper-button.style-destructive')
  }

  if (isOnChannelSite()) {
    return document.querySelector('div.ytd-c4-tabbed-header-renderer:nth-child(4) > ytd-button-renderer:nth-child(1) > a:nth-child(1) > paper-button:nth-child(1)')
  }
}

function getSubscribeButtonLabel() {
  if (isOnWatchSite()) {
    return document.querySelector('yt-formatted-string.style-destructive')
  }

  if (isOnChannelSite()) {
    return document.querySelector('div.ytd-c4-tabbed-header-renderer:nth-child(4) > ytd-button-renderer:nth-child(1) > a:nth-child(1) > paper-button:nth-child(1) > yt-formatted-string:nth-child(1)')
  }
}

function applySubscribedStyle() {
  getSubscribeButton().style.backgroundColor = 'green'
  getSubscribeButtonLabel().innerText = 'Subscribed'
}

function applyUnsubscribedStyle() {
  getSubscribeButton().style.backgroundColor = '#8d20ae'
  getSubscribeButtonLabel().innerText = 'Subscribe'
}

async function detectSubscription() {
  const channelId = getChannelId()
  const { channels = {} } = await browser.storage.local.get()
  let isSubscribed = Object.keys(channels).includes(channelId)

  if (isSubscribed) {
    applySubscribedStyle()
  } else {
    applyUnsubscribedStyle()
  }

  getSubscribeButton().addEventListener('click', async (e) => {
    e.preventDefault()

    const { channels: next = {} } = await browser.storage.local.get()
    isSubscribed = Object.keys(next).includes(channelId)

    if (isSubscribed) {
      applyUnsubscribedStyle()
      browser.storage.local.set({
        channels: Object.keys(next).reduce((accumulator, id) => {
          if (id === channelId) {
            return accumulator
          }

          return { ...accumulator, [id]: next[id] }
        }, {})
      })
    } else {
      applySubscribedStyle()
      browser.storage.local.set({
        channels: { ...next, [channelId]: getChannelName() },
      })
    }
  })
}

let prevHref
setInterval(() => {
  if (prevHref !== location.href) {
    prevHref = location.href

    // It's a nasty race condition.
    // TODO: A change detection system is needed here.
    setTimeout(detectSubscription, 3000)
  }
}, 100)
