# CHANGELOG

## 0.4.0 - 2020/11/03

### Breaking

### Fixes

- Fix build instruction documentation in README

### Features

### Additional

- Use npm modules instead of manually downloaded third party files

## 0.3 - 2020/11/01

### Breaking

### Fixes

- Correct the way of getting the latest version number

### Features

### Additional

- Replace moment.js with dayjs
- Use safe methods to inject video information into popup DOM

## 0.2 - 2020/05/02

### Breaking

### Fixes

- Better page navigation detection system

### Features

### Additional

## 0.1 - 2020/05/01

### Breaking

- Initial commit

### Fixes

### Features

### Additional
