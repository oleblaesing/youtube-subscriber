class EmptyError extends Error {}
class ParsingError extends Error {}
class InvalidChannelIdsError extends Error {
  constructor(invalidChannelIds) {
    super()
    this.invalidChannelIds = invalidChannelIds
  }
}

browser.storage.local.get().then(({ channels = {} }) => {
  document.getElementById('download-backup').addEventListener('click', () => {
    const a = document.createElement('a')
    a.href = URL.createObjectURL(new Blob([JSON.stringify(channels)]))
    a.download = 'YouTubeSubscriberBackup.json'
    a.type = 'application/json'
    a.click()
  })

  const applyBackupInput = document.getElementById('apply-backup-input')
  applyBackupInput.addEventListener('keydown', () => applyBackupInput.setCustomValidity(''))
  applyBackupInput.addEventListener('focus', () => {
    applyBackupInput.value = ''
  })

  if (Object.keys(channels).length > 0) {
    applyBackupInput.value = JSON.stringify(channels)
  }

  const applyBackupButton = document.getElementById('apply-backup-button')

  document.getElementById('apply-backup-form').addEventListener('submit', async (e) => {
    e.preventDefault()
    e.stopPropagation()

    applyBackupInput.readOnly = true
    applyBackupButton.disabled = true

    try {
      if (applyBackupInput.value === '') {
        throw new EmptyError()
      }

      let backup
      let keys

      try {
        backup = JSON.parse(applyBackupInput.value)

        if (backup.length !== undefined) {
          throw new ParsingError()
        }

        keys = Object.keys(backup)
      } catch (_) {
        throw new ParsingError()
      }

      const invalidChannelIds = keys.filter(id => id.length < 9)

      if ((keys.length > 0) && (invalidChannelIds.length > 0)) {
        throw new InvalidChannelIdsError(invalidChannelIds)
      }

      await browser.storage.local.set({ channels: backup })
    } catch (error) {
      let errorMessage = error.message || 'An error occured while applying your backup'

      if (error instanceof EmptyError) {
        errorMessage = 'You need to paste in any backup data first'
      }

      if (error instanceof ParsingError) {
        errorMessage = 'This backup seems not to be well formatted'
      }

      if (error instanceof InvalidChannelIdsError) {
        if (error.invalidChannelIds.length === 1) {
          errorMessage = `${error.invalidChannelIds[0]} is not a valid channel id`
        } else {
          errorMessage = `${error.invalidChannelIds.slice(0, -1).join(', ')} and ${error.invalidChannelIds[error.invalidChannelIds.length - 1]} are not valid channel ids`
        }
      }

      applyBackupInput.setCustomValidity(errorMessage)
    } finally {
      applyBackupInput.readOnly = false
      applyBackupButton.disabled = false
    }
  })
})
