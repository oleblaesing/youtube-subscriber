#!/usr/bin/env sh

npm ci

cp node_modules/dayjs/dayjs.min.js src/modules/third-party
cp node_modules/numeral/min/numeral.min.js src/modules/third-party
cp node_modules/dayjs/plugin/relativeTime.js src/modules/third-party

version="$(git describe --exact-match --tags $(git log -n1 --pretty='%h'))"

zip -r -FS "youtube-subscriber-$version.zip" * \
  -x node_modules/\* \
  -x .editorconfig \
  -x .git \
  -x .gitignore \
  -x build.sh \
  -x CHANGELOG.md \
  -x LICENSE \
  -x README.md \
  -x youtube-subscriber*.zip
