async function fetchVideos(channels) {
  const chunks = await Promise.all(Object.keys(channels).map(async (id) => {
    const response = await fetch(`https://www.youtube.com/feeds/videos.xml?channel_id=${id}`, {
      headers: { Accept: 'text/xml' },
    })

    const xml = (new DOMParser()).parseFromString(await response.text(), 'text/xml')

    return Array.from(xml.getElementsByTagName('entry')).map(entry => ({
      author: entry.getElementsByTagName('author')[0].getElementsByTagName('name')[0].innerHTML,
      id: entry.getElementsByTagName('yt:videoId')[0].innerHTML,
      publishedAt: new Date(entry.getElementsByTagName('published')[0].innerHTML),
      thumbnail: entry.getElementsByTagName('media:group')[0].getElementsByTagName('media:thumbnail')[0].getAttribute('url'),
      title: entry.getElementsByTagName('title')[0].innerHTML,
      views: entry.getElementsByTagName('media:community')[0].getElementsByTagName('media:statistics')[0].getAttribute('views'),
    }))
  }))

  return chunks.reduce((videos, chunk) => videos.concat(chunk), [])
}
