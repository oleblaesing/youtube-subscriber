# YouTube Subscriber

Subscribe to YouTube channels without needing a Google account.

Currently only Firefox based Browsers are supported.

Just install the plugin and start subscribing to YouTube channels. Only subscribe buttons on video- or channelpages will currently be recognized.

You can also download and apply backups inside the extensions preferences tabs ([about:addons](about:addons)).

## For developers

To load your local directory into your browser, run `./build.sh` once (to copy the third-party modules) and visit the `about:debugging` page. Click on `This Firefox` there. In the next page click on `Load Temporary Add-on...` and select the `manifest.json` file of the extension.

You can inspect everything related to the extension via the `Inspect` button.

All the subscriptions are saved inside the Extentions Storage which you can see via `Storage > Extension Storage > moz-extension://*`.

Some more resources to learn from:

- https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/Your_first_WebExtension
- https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions/API/storage

### Release a new version

Checkout the `master` branch and get the latest version number.

```sh
git checkout master
git tag
```

Look through all the changes that have been made to the codebase since that version:

```sh
git log 0.0..HEAD
```

Take these commit headlines and create a new `CHANGELOG.md` on-top-entry with them.

```md
## 0.1 - 2020/05/01

### Breaking

- Initial commit

### Fixes

### Features

### Additional
```

Bump the version number in the `manifest.json`, `package.json` and `package-lock.json` file.

Commit those changes with a commit message in this format:

```
Release of version 0.1
```

Now create the new git tag:

```sh
git tag 0.1
```

..and run the build script to create the zip file:

```sh
./build.sh
```

You now have to upload the zip file to [https://addons.mozilla.org/en-US/developers/](https://addons.mozilla.org/en-US/developers/).

Some more resources to learn from:

- https://extensionworkshop.com/documentation/publish/package-your-extension/
