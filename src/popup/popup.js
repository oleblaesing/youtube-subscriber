dayjs.extend(window.dayjs_plugin_relativeTime)

const loader = document.createElement('div')
loader.id = 'loader'
document.body.appendChild(loader)

browser.storage.local.get().then(async ({ channels = {} }) => {
  browser.storage.local.set({ lastOpened: +(new Date()) })
  browser.browserAction.setBadgeText({ text: '' })

  if (Object.keys(channels).length === 0) {
    const p = document.createElement('p')
    p.innerHTML = `You aren't subscribed to any channel yet. Go ahead and subscribe to some or <a id="open-extension-settings" href="#">open your extension settings</a> and apply some backup data!`
    document.body.appendChild(p)

    document.getElementById('open-extension-settings').addEventListener('click', (e) => {
      e.preventDefault()

      browser.runtime.openOptionsPage()
    })
  } else {
    const videoList = document.getElementById('video-list')

    let active = -1
    const chunks = chunkify(
      (await fetchVideos(channels)).sort((a, b) => b.publishedAt - a.publishedAt),
      10,
    )

    function scroll() {
      if ((window.scrollY + window.innerHeight) >= document.body.clientHeight) {
        window.removeEventListener('scroll', scroll)

        if (active < (chunks.length - 1)) {
          // We're rendering later chunks
          if (videoList.hasChildNodes()) {
            document.body.appendChild(loader)
          }

          chunks[++active].forEach((video) => {
            const viewsFormatted = numeral(video.views).format('0.0a').replace('.0', '').toUpperCase()
            const publishedFormatted = dayjs(video.publishedAt).fromNow()

            const thumbnail = document.createElement('div')
            thumbnail.id = 'img'
            thumbnail.style.backgroundImage = `url("${video.thumbnail}")`

            const title = document.createElement('span')
            title.id = 'title'
            title.textContent = video.title

            const author = document.createElement('span')
            author.textContent = video.author

            const views = document.createElement('span')
            views.textContent = `${viewsFormatted} • ${publishedFormatted}`

            const information = document.createElement('div')
            information.id = 'information'
            information.appendChild(title)
            information.appendChild(author)
            information.appendChild(views)

            const a = document.createElement('a')
            a.href = `https://www.youtube.com/watch?v=${video.id}`
            a.className = 'video'
            a.appendChild(thumbnail)
            a.appendChild(information)

            const videoItem = document.createElement('li')
            videoItem.appendChild(a)

            videoList.appendChild(videoItem)
          })

          window.addEventListener('scroll', scroll)

          loader.remove()
        }
      }
    }

    window.addEventListener('scroll', scroll)

    // Render first chunk
    scroll()
  }

  loader.remove()
})
