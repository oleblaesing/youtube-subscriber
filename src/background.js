async function updateBadge({
  channels = {},
  lastOpened = +(new Date(Date.now() - (1000 * 60 * 60 * 24))),
}) {
  const unseen = (await fetchVideos(channels))
    .filter(({ publishedAt }) => publishedAt > new Date(lastOpened))
    .length

  browser.browserAction.setBadgeBackgroundColor({ color: '#666666' })
  browser.browserAction.setBadgeTextColor({ color: 'white' })
  browser.browserAction.setBadgeText({
    text: (unseen > 0) ? `${unseen}` : '',
  })
}

browser.storage.local.get().then(updateBadge)
setInterval(() => browser.storage.local.get().then(updateBadge), 1000 * 60 * 3)
